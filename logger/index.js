const winston = require('winston');

//formatting for logger
const { combine, timestamp, colorize, json, printf } = winston.format;
const myFormat = combine(
  colorize(),
  timestamp(),
  printf(info => {
    return `${info.timestamp} :: ${info.message}`;
  })
  // json(),
);

//logger
const logger = winston.createLogger({
  level: 'info',
  format: myFormat,

  transports: [
    new winston.transports.File({
      filename: './logger/error.log',
      level: 'error',
      handleExceptions: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
    }),
    new winston.transports.File({
      filename: './logger/info.log',
      handleExceptions: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
    }),
  ],
  exitOnError: false,
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'test') {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
      level: 'debug',
      handleExceptions: true,
      colorize: true,
    })
  );
}

module.exports = logger;
module.exports.stream = {
  write: function(message, encoding) {
    logger.info(message);
  },
  log: message => {
    const guild = message.guild ? message.guild : 'DM';
    const channel = guild !== 'DM' ? message.channel : '';
    const channelName = channel ? channel.name : '';

    let preparedMessage = `${guild} :: ${channel}${channelName} :: ${
      message.author
    }${message.author.username} :: ${message}`;

    logger.info(preparedMessage);
  },
};
