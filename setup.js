const util = require('util');
const exec = util.promisify(require('child_process').exec);
const spawn = util.promisify(require('child_process').spawn);

async function migrationsAndStart() {
  try {
    // await exec('yarn migrate:run');
    // console.log('migration finished');
    console.log('starting app...');
    exec('yarn start', { maxBuffer: 1024 * 500 });
    // spawn('yarn start');
  } catch (e) {
    console.log(e);
  }
}

migrationsAndStart();
