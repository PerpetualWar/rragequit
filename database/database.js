const debug = process.env.NODE_ENV === 'development' ? true : false;

const database = require('knex')({
  client: 'pg',
  debug,
  asyncStackTraces: debug,
  connection: {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
  },
});

module.exports = database;
