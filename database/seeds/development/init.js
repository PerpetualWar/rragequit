exports.seed = async function(knex, Promise) {
  // Deletes ALL existing entries
  return Promise.all([
    await knex('guilds').del(),
    await knex('channels').del(),
    await knex('gametypes').del(),
    await knex('players').del(),
    await knex('added_players').del(),
    // Inserts seed entries
    await knex('guilds').insert({
      id: '501089528480661505',
      // name: 'Rragequit',
      prefix: '!',
      admin_role: 'admin',
    }),
    await knex('guilds').insert({
      id: '150610549829271552',
      // name: '#capickup',
      prefix: '!',
      admin_role: 'admin',
    }),
    // Inserts seed entries
    await knex('channels').insert({
      id: '501821405822713856',
      // name: 'pickup',
      guild_id: '501089528480661505',
    }),
    await knex('channels').insert({
      id: '150610797117046784',
      // name: 'capickup',
      guild_id: '150610549829271552',
    }),
    await knex('channels').insert({
      id: '501082090427645962',
      // name: 'testing',
      guild_id: '150610549829271552',
    }),
    // Inserts seed entries
    await knex('gametypes').insert({
      id: 1,
      name: 'qltdm',
      number_of_players: 8,
      channel_id: '150610797117046784',
    }),
    await knex('gametypes').insert({
      id: 2,
      name: 'qlctf',
      number_of_players: 10,
      channel_id: '150610797117046784',
    }),
    await knex('gametypes').insert({
      id: 3,
      name: '2v2',
      number_of_players: 4,
      channel_id: '501821405822713856',
    }),
    await knex('gametypes').insert({
      id: 4,
      name: 'q3',
      number_of_players: 8,
      channel_id: '501821405822713856',
    }),
    // Inserts seed entries
    await knex('players').insert({
      id: '103562232985571328',
      // username: 'PerpetualWar',
    }),
    await knex('players').insert({
      id: '480995588729733120',
      // username: 'Eun0mia',
    }),
    await knex('players').insert({
      id: '492647647723913217',
      // username: 'salcox',
    }),
    // Inserts seed entries
    await knex('added_players').insert({
      player_id: '103562232985571328',
      // player_username: 'PerpetualWar',
      guild_id: '150610549829271552',
      channel_id: '150610797117046784',
      gametype_id: 1,
    }),
    await knex('added_players').insert({
      player_id: '480995588729733120',
      // player_username: 'Eun0mia',
      guild_id: '501089528480661505',
      channel_id: '501821405822713856',
      gametype_id: 3,
    }),
    await knex('added_players').insert({
      player_id: '492647647723913217',
      // player_username: 'salcox',
      guild_id: '150610549829271552',
      channel_id: '150610797117046784',
      gametype_id: 2,
    }),
  ]);
};
