const { onUpdateTrigger } = require('../knexfile');

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable('banned_players', table => {
        table.increments();
        table
          .bigInteger('player_id')
          // .string('player_id', 20)
          .references('id')
          .inTable('players')
          .onDelete('cascade');
        table
          .bigInteger('channel_id')
          // .string('channel_id', 20)
          .references('id')
          .inTable('channels')
          .onDelete('cascade');
        table
          .bigInteger('guild_id')
          // .string('guild_id', 20)
          .references('id')
          .inTable('guilds')
          .onDelete('cascade');
        table.string('banned_reason');
        table.bigInteger('banned_for');
        table.timestamp('unbanned_at');
        // table.timestamp('banned_at');
        table.timestamps(true, true);
        // table.unique(['player_id', 'channel_id']);
      })
      .then(() => knex.raw(onUpdateTrigger('banned_players'))),
    knex.schema
      .createTable('muted_players', table => {
        table.increments();
        table
          .bigInteger('player_id')
          // .string('player_id', 20)
          .references('id')
          .inTable('players')
          .onDelete('cascade');
        table
          .bigInteger('channel_id')
          // .string('channel_id', 20)
          .references('id')
          .inTable('channels')
          .onDelete('cascade');
        table
          .bigInteger('guild_id')
          // .string('guild_id', 20)
          .references('id')
          .inTable('guilds')
          .onDelete('cascade');
        table.string('muted_reason');
        table.bigInteger('muted_for');
        table.timestamp('unmuted_at');
        // table.timestamp('muted_at');
        table.timestamps(true, true);
        // table.unique(['player_id', 'channel_id']);
      })
      .then(() => knex.raw(onUpdateTrigger('muted_players'))),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('muted_players'),
    knex.schema.dropTable('banned_players'),
  ]);
};
