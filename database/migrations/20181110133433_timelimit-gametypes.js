exports.up = function(knex, Promise) {
  return knex.schema.table('gametypes', table => {
    table.bigInteger('auto_remove_limit');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('gametypes', table => {
    table.dropColumn('auto_remove_limit');
  });
};
