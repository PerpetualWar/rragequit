const { onUpdateTrigger } = require('../knexfile');

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable('maps', table => {
        table.increments();
        table.text('maps');
        table
          .bigInteger('gametype_id')
          .references('id')
          .inTable('gametypes')
          .onDelete('cascade')
          .unique();
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('maps'))),
    knex.schema
      .createTable('rules', table => {
        table.increments();
        table.string('rules');
        table
          .bigInteger('guild_id')
          .references('id')
          .inTable('guilds')
          .onDelete('cascade')
          .unique();
        table
          .bigInteger('channel_id')
          .references('id')
          .inTable('channels')
          .onDelete('cascade');
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('rules'))),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('rules'),
    knex.schema.dropTable('maps'),
  ]);
};
