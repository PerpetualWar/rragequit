exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table
        .bigInteger('guild_id')
        .references('id')
        .inTable('guilds')
        .onDelete('cascade');
      table.string('alias', 20);
      table.boolean('assigned').defaultTo(false);
      table.unique(['name', 'guild_id']);
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table.dropColumn('assigned');
      table.dropColumn('alias');
      table.dropColumn('guild_id');
    }),
  ]);
};
