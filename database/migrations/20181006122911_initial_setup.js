const { onUpdateTrigger } = require('../knexfile');

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable('guilds', table => {
        table
          .bigInteger('id')
          // .string('id', 20)
          .unique()
          .primary();
        // table.string('name', 80);
        table.string('prefix', 1);
        table.string('admin_role', 80);
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('guilds'))),
    knex.schema
      .createTable('channels', table => {
        table
          .bigInteger('id')
          // .string('id', 20)
          .unique()
          .primary();
        // table.string('name', 80);
        table
          .bigInteger('guild_id')
          // .string('guild_id', 20)
          .references('id')
          .inTable('guilds')
          .onDelete('cascade');
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('channels'))),
    knex.schema
      .createTable('gametypes', table => {
        table
          .increments('id')
          .unique()
          .primary();
        table.string('name', 80);
        table.integer('number_of_players');
        table
          .bigInteger('channel_id')
          // .string('channel_id', 20)
          .references('id')
          .inTable('channels')
          .onDelete('cascade');
        table.timestamps(true, true);
        table.unique(['name', 'channel_id']);
      })
      .then(() => knex.raw(onUpdateTrigger('gametypes'))),
    knex.schema
      .createTable('players', table => {
        table
          .bigInteger('id')
          // .string('id', 20)
          .unique()
          .primary();
        // table.string('username', 80);
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('players'))),
    knex.schema
      .createTable('added_players', table => {
        table.increments();
        table
          .bigInteger('player_id')
          // .string('player_id', 20)
          .references('id')
          .inTable('players')
          .onDelete('cascade');
        table
          .bigInteger('guild_id')
          // .string('guild_id', 20)
          .references('id')
          .inTable('guilds')
          .onDelete('cascade');
        table
          .bigInteger('channel_id')
          // .string('channel_id', 20)
          .references('id')
          .inTable('channels')
          .onDelete('cascade');
        table
          .integer('gametype_id')
          .references('id')
          .inTable('gametypes')
          .onDelete('cascade')
          .notNull();
        table.timestamps(true, true);
        table.unique(['player_id', 'gametype_id']);
      })
      .then(() => knex.raw(onUpdateTrigger('added_players'))),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('added_players'),
    knex.schema.dropTable('players'),
    knex.schema.dropTable('gametypes'),
    knex.schema.dropTable('channels'),
    knex.schema.dropTable('guilds'),
  ]);
};
