const { onUpdateTrigger } = require('../knexfile');

exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema
      .createTable('servers', table => {
        table
          .increments()
          .unique()
          .primary();
        table.string('ip').unique();
        table.string('password', 80);
        table
          .bigInteger('gametype_id')
          .references('id')
          .inTable('gametypes')
          .onDelete('cascade');
        table.timestamps(true, true);
      })
      .then(() => knex.raw(onUpdateTrigger('servers'))),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([knex.schema.dropTable('servers')]);
};
