exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table.string('name').unique();
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table.dropColumn('name');
    }),
  ]);
};
