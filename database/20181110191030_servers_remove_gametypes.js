exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table.dropColumn('gametype_id');
    }),
  ]);
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.table('servers', table => {
      table
        .bigInteger('gametype_id')
        .references('id')
        .inTable('gametypes')
        .onDelete('cascade');
    }),
  ]);
};
