const database = require('../database');

module.exports = async playerId => {
  return await database('added_players')
    .delete()
    .where({
      player_id: playerId,
    });
};
