const database = require('../database');

module.exports = async guildId => {
  if (guildId)
    return await database
      .from('channels')
      .where({ guild_id: guildId })
      .select();

  return await database.from('channels').select();
};
