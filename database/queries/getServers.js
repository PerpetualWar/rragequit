const database = require('../database');

module.exports = async (name, gametypeId, guildId) => {
  console.log('name :', name);
  try {
    if (name && gametypeId && guildId)
      return await database
        .from('servers')
        .where({ name, gametype_id: gametypeId, guild_id: guildId })
        .select();

    return await database.from('servers').select();
  } catch (e) {
    console.error(e);
  }
};
