const database = require('../database');

module.exports = async (ip, name, password, guildId) => {
  return await database('servers')
    .insert({
      ip,
      name,
      password,
      guild_id: guildId,
    })
    .returning('id');
};
