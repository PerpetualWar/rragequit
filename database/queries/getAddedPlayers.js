const database = require('../database');

module.exports = async (channelId, gametypeId) => {
  if (channelId && gametypeId)
    return await database
      .select()
      .from('added_players')
      .where({
        channel_id: channelId,
        gametype_id: gametypeId,
      });
  if (channelId)
    return await database
      .select()
      .from('added_players')
      .where({
        channel_id: channelId,
      });

  return await database.select().from('added_players');
};
