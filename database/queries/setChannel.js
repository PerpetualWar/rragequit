const database = require('../database');

module.exports = async (guildId, channel) => {
  try {
    return await database('channels')
      .insert({
        id: channel.id,
        guild_id: guildId,
      })
      .returning('id');
  } catch (e) {
    if (
      e.message
        .toLowerCase()
        .indexOf('duplicate key value violates unique constraint') >= 0
    ) {
      console.log('works');
    }
    throw new Error('Channel already exist in db');
  }
};
