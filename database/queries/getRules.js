const database = require('../database');

module.exports = async guildId => {
  try {
    if (guildId) {
      return await database
        .from('rules')
        .where({ guild_id: guildId })
        .select();
    }
    return await database('rules').select();
  } catch (e) {
    console.error(e);
  }
};