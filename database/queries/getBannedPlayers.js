const database = require('../database');

module.exports = async (channelId, playerId) => {
  if (channelId && playerId)
    return await database
      .select()
      .from('banned_players')
      .where({
        channel_id: channelId,
        player_id: playerId,
        unbanned_at: null,
      });
  if (channelId)
    return await database
      .select()
      .from('banned_players')
      .where({
        channel_id: channelId,
        unbanned_at: null,
      });

  return await database
    .select()
    .from('banned_players')
    .where({ unbanned_at: null });
};
