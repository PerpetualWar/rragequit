const database = require('../../database');

module.exports = async () => {
  return await database('gametypes')
    .join('added_players', { 'gametypes.id': 'added_players.gametype_id' })
    .select(
      'gametypes.auto_remove_limit',
      'added_players.created_at',
      'added_players.gametype_id',
      'added_players.player_id',
      'added_players.channel_id'
    );
};
