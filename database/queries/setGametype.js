const database = require('../database');
const dbError = require('../../exceptions/dbError');
const msConverter = require('../../app/utils/timedUserAction');

module.exports = async (
  guildId,
  [name, numberOfPlayers, channelId, autoRemoveLimit]
) => {
  console.log(name, numberOfPlayers, channelId, autoRemoveLimit);
  const ms = msConverter(autoRemoveLimit);
  console.log('ms :', ms);

  try {
    await database('gametypes')
      .insert({
        name,
        number_of_players: numberOfPlayers,
        channel_id: channelId,
        auto_remove_limit: ms ? ms : 2700000, //45mins: 2700000ms
      })
      .returning('id');
  } catch (e) {
    console.log('from setGametypes', e.message);
    if (
      e.message
        .toLowerCase()
        .indexOf('duplicate key value violates unique constraint') >= 0
    ) {
      console.log('works');
      throw new Error('Gametype already exist in db');
      // throw new dbError('Gametype already exist in db');
    }
  }
};
