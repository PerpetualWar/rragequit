const database = require('../database');
const dbError = require('../../exceptions/dbError');

module.exports = async (name, channelId) => {
  try {
    await database('gametypes')
      .where({
        name,
        channel_id: channelId,
      })
      .del();
  } catch (e) {
    console.log('from setGametypes', e.message);

    console.log('works');
    throw new Error('Something went wrong with delete in db');
  }
};
