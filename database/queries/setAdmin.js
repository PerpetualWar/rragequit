const database = require('../database');

module.exports = async (guildId, adminRole) => {
  try {
    return await database('guilds')
      .where({ id: guildId })
      .update({ admin_role: adminRole })
      .returning('id');
  } catch (e) {
    if (
      e.message
        .toLowerCase()
        .indexOf('duplicate key value violates unique constraint') >= 0
    ) {
      console.log('works');
    }
    throw new Error('Admin role already exist in db');
  }
};
