const database = require('../database');

module.exports = async (
  table,
  playerId,
  channelId,
  guildId,
  actionFor,
  actionReason
) => {
  const reason = `${table}_reason`;
  const time = `${table}_for`;

  try {
    const bleh = await database('players')
      .select('id')
      .where({ id: playerId });
    console.log('bleh :', bleh);
    if (bleh.length !== 0)
      await database(`${table}_players`).insert({
        player_id: playerId,
        channel_id: channelId,
        guild_id: guildId,
        [reason]: actionReason,
        [time]: actionFor,
      });
    else {
      await database('players').insert({ id: playerId });
      await database(`${table}_players`).insert({
        player_id: playerId,
        channel_id: channelId,
        guild_id: guildId,
        [reason]: actionReason,
        [time]: actionFor,
      });
    }
  } catch (e) {
    console.error(e);
    // if (
    //   e.message
    //     .toLowerCase()
    //     .indexOf(
    //       'duplicate key value violates unique constraint "players_id_unique"'
    //     ) >= 0
    // ) {
    //   message.channel.send('Gametype does not exist!');
    // }
  }
};
