const database = require('../database');

module.exports = async playerId => {
  if (playerId)
    return await database
      .from('players')
      .where({ id: playerId })
      .select();

  return await database.from('players').select();
};
