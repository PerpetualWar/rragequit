const database = require('../database');

module.exports = async (channelId, gametypeId, gametypeName) => {
  try {
    if (channelId && gametypeId)
      return await database
        .from('gametypes')
        .where({ channel_id: channelId, id: gametypeId })
        .select();

    if (channelId && gametypeName)
      return await database
        .from('gametypes')
        .where({ channel_id: channelId, name: gametypeName })
        .select();

    if (channelId)
      return await database
        .from('gametypes')
        .where({ channel_id: channelId })
        .select();
    return await database.from('gametypes').select();
  } catch (e) {
    console.log(e);
  }
};
