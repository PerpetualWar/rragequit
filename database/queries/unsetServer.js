const database = require('../database');
const dbError = require('../../exceptions/dbError');

module.exports = async (name, guildId) => {
  try {
    await database('servers')
      .where({
        name,
        guild_id: guildId,
      })
      .del();
  } catch (e) {
    throw new Error('Something went wrong with delete in db');
  }
};
