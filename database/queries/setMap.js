const database = require('../database');

module.exports = async (maps, gametypeId) => {
  try {
    await database('maps')
      .insert({
        maps,
        gametype_id: gametypeId,
      })
      .returning('id');
  } catch (e) {
    console.log(e.message)
    if (
      e.message
        .toLowerCase()
        .indexOf('duplicate key value violates unique constraint') >= 0
    ) {
      console.log('it happened')
      await database('maps')
        .update({ maps })
        .where({ gametype_id: gametypeId });
    }
  }
};
