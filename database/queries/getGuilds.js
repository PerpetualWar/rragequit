const database = require('../database');

module.exports = async guildId => {
  if (guildId)
    return await database
      .from('guilds')
      .where({ id: guildId })
      .select();
  return await database.from('guilds').select();
};
