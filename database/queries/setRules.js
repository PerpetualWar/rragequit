const database = require('../database');

module.exports = async (rules, guildId) => {
  try {
    await database('rules')
      .insert({
        rules,
        guild_id: guildId,
      })
      .returning('id');
  } catch (e) {
    console.log(e.message)
    if (
      e.message
        .toLowerCase()
        .indexOf('duplicate key value violates unique constraint') >= 0
    ) {
      console.log('it happened')
      await database('rules')
        .update({ rules })
        .where({ guild_id: guildId });
    } else throw new Error(e);
  }
};