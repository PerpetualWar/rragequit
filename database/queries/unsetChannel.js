const database = require('../database');

module.exports = async (guildId, channel) => {
  try {
    await database('channels')
      .where({
        id: channel.id,
        guild_id: guildId,
      })
      .del();
  } catch (e) {
    throw new Error('Something went wrong with delete in db');
  }
};
