const database = require('../database');

module.exports = async (channelId, gametypeId) => {
  return await database('gametypes')
    .join('added_players', { 'gametypes.id': 'added_players.gametype_id' })
    .select(
      'added_players.gametype_id',
      'gametypes.name',
      'gametypes.number_of_players',
      'added_players.player_id'
    );
  // if (channelId && gametypeId)
  //   return await database
  //     .from('gametypes')
  //     .where({ channel_id: channelId, id: gametypeId })
  //     .select();
  // if (channelId)
  //   return await database
  //     .from('gametypes')
  //     .where({ channel_id: channelId })
  //     .select();
  // return await database.from('gametypes').select();
};
