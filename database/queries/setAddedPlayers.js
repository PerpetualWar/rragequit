const database = require('../database');

module.exports = async (id, guildId, channelId, gametypeId) => {
    return await database('added_players')
     .insert({
       player_id:id,
       guild_id:guildId,
       channel_id:channelId,
       gametype_id:gametypeId
     });
};