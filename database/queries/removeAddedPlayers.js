const database = require('../database');

module.exports = async (playerId, gametypeId) => {
  return await database('added_players')
    .where({
      player_id: playerId,
      gametype_id: gametypeId,
    })
    .delete();
};
