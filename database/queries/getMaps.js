const database = require('../database');

module.exports = async gametypeId => {
  try {
    if (gametypeId) {
      return await database
        .from('maps')
        .where({ gametype_id: gametypeId })
        .select();
    }
    return await database('maps').select();
  } catch (e) {
    console.error(e);
  }
};
