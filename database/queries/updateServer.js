const database = require('../database');

module.exports = async (id, assigned) => {
  return await database('servers')
    .where({
      id,
    })
    .update({ assigned });
};
