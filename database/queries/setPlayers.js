const database = require('../../database/database');

module.exports = async id => {
  try {
    if (typeof id === 'string') {
      await database('players')
        .insert({
          id,
        })
        .returning('id');
    } else {
      await database('players').insert(id);
      console.log('after insert');
    }
  } catch (e) {
    console.error(e);
  }
};
