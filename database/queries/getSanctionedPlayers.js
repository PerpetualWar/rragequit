const database = require('../database');

module.exports = async (table, channelId, playerId) => {
  const columnName = `un${table}_at`;

  if (channelId && playerId)
    return await database
      .select()
      .from(`${table}_players`)
      .where({
        channel_id: channelId,
        player_id: playerId,
        [columnName]: null,
      });
  if (channelId)
    return await database
      .select()
      .from(`${table}_players`)
      .where({ channel_id: channelId, [columnName]: null });

  return await database
    .select()
    .from(`${table}_players`)
    .where({ [columnName]: null });
};
