const setTopic = require('../utils/topicSetter');
const removePlayer = require('../../database/queries/removePlayer');
const getPlayers = require('../../database/queries/getAddedPlayers');
const printPlayers = require('../utils/printPlayers');
const logger = require('../../logger');

module.exports = client =>
  client.on('presenceUpdate', async (oldVal, newVal) => {
    console.log('oldVal :', oldVal);
    console.log('newVal :', newVal);
    // logger.info(
    //   `${newVal} :: ${newVal.user.username} :: ${newVal.presence.status}`
    // );

    try {
      //get all players and get id of players
      const userId = newVal.id;
      const addedPlayers = await getPlayers();
      console.log('addedPlayers :', addedPlayers);

      //if user is offline, check if it was a player and remove it from db,
      //set topic and send channel message again to notify
      //otherwise do nothing
      // status === 'idle' can also be used
      if (newVal.presence.status === 'offline') {
        addedPlayers.forEach(async player => {
          if (player.player_id === userId) {
            player.channel_id;
            console.log('from offline');
            const channel = newVal.guild.channels.get(player.channel_id);
            console.log('channels :', channel);
            await removePlayer(userId);
            setTopic(channel);
            printPlayers(channel);
          }
        });
      }
    } catch (e) {
      console.error(e);
    }
  });
