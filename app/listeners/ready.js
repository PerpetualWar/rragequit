const setTopic = require('../utils/topicSetter');
const pollingActions = require('../utils/pollingActions');
// const database = require('../../database/database');
// const getGametypes = require('../../database/queries/getGametypes');
// const getGuilds = require('../../database/queries/getGuilds');
const getChannels = require('../../database/queries/getChannels');
const logger = require('../../logger');

module.exports = client =>
  client.on('ready', async () => {
    console.log('Bot is online!', client);
    logger.info(`Bot is online!`);
    const botGuilds = client.guilds;

    botGuilds.forEach(async (value, key) => {
      console.log('value :', key, value);
      const channels = value.channels;
      const regChannels = await getChannels(key);

      //set topic for all registered channels
      channels.forEach(channel => {
        regChannels.forEach(regChannel => {
          if (channel.id === regChannel.id) {
            setTopic(channel);
          }
        });
      });
    });

    //interval for checking banned and muted players
    setInterval(() => {
      pollingActions(client);
    }, 300000); //10mins: 600000, 5mins: 300000
  });
