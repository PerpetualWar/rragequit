const cooldownLogic = require('./../utils/cooldowns');
const guildOnly = require('../utils/guildOnly');
const dmOnly = require('../utils/dmOnly');
const commandRunner = require('../utils/commandRunner');
const getChannels = require('../../database/queries/getChannels');
const getGuilds = require('../../database/queries/getGuilds');
const globalCommands = require('../utils/globalCommands');
const logger = require('../../logger');

module.exports = (client, cooldowns) =>
  client.on('message', async message => {
    logger.stream.log(message);

    if (message.guild) {
      var [guild, channels] = await Promise.all([
        getGuilds(message.guild.id),
        getChannels(message.guild.id),
      ]);
      var [{ prefix }] = guild;
    }

    //if no prefix or bot sending msg, ignore
    if (!message.content.startsWith(prefix || '!') || message.author.bot)
      return;

    //we take out command name and possible arguments
    const args = message.content.slice(prefix ? prefix.length : 1).split(/ +/);
    const commandName = args.shift().toLowerCase();

    //get command object from collection by command name or alias
    const command =
      client.commands.get(commandName) ||
      client.commands.find(
        cmd => cmd.aliases && cmd.aliases.includes(commandName)
      );

    if (!command) return;

    //sanity checks
    const guildMsg = guildOnly(command, message);
    const dmMsg = dmOnly(command, message);
    if (guildMsg || dmMsg) return;

    //global commands allowed from every guild channel for admin role
    if (globalCommands.includes(command.name))
      return commandRunner(command, message, args);

    //all other commands listen only on designated channels or dms
    if (
      message.channel.type !== 'dm' &&
      channels.every(item => item.id !== message.channel.id)
    )
      return;

    //spam prevention
    const inEffect = cooldownLogic(command, message);
    if (inEffect) return;
    console.log('here');

    //here we finally run commands
    commandRunner(command, message, args);
  });
