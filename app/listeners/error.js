const logger = require('../../logger');

module.exports = client => {
  client.on('error', error => {
    logger.info(error);
    console.error(error);
  });
};
