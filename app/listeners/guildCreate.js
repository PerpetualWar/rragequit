const setGuild = require('../../database/queries/setGuild');
const setChannel = require('../../database/queries/setChannel');
const logger = require('../../logger');

module.exports = client =>
  client.on('guildCreate', async guild => {
    console.log('guild', guild);
    logger.info(`New guild created: ${guild}`);

    try {
      await setGuild(guild);
    } catch (e) {
      console.log('Error when adding new guild to db');
    }
  });
