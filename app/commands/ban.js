const getMember = require('../utils/getMemberObject');
const ttl = require('../utils/timedUserAction');
const ttlInsert = require('../../database/queries/ttlInsert');
const database = require('../../database/database');

module.exports = {
  name: 'ban',
  description: 'Ban given player at channel',
  aliases: ['b'],
  allArguments: ['<player name>', '<ban timeframe>', '<ban reason>'],
  admin: true,
  guildOnly: true,
  async execute(message, args) {
    if (args.length > 1) {
      const {
        id,
        user: { username },
      } = getMember(message, args, true);

      const banReason = args.slice(2).join(' ');
      const channelId = message.channel.id;
      const guildId = message.guild.id;

      try {
        //first check if player already have an active ban
        const bannedPlayer = await database('banned_players')
          .where({
            unbanned_at: null,
            player_id: id,
            channel_id: channelId,
          })
          .select();

        //if active ban, just exit early and inform user
        if (bannedPlayer.length > 0)
          return message.reply(' player is already banned');

        console.log('bannedPlayer :', bannedPlayer);

        const banMember =
          message.channel.members.get(id) ||
          (await message.guild.fetchMember(id));

        const ttlValue = ttl(args[1]);
        console.log(ttlValue);

        await ttlInsert('banned', id, channelId, guildId, ttlValue, banReason);

        await message.channel.overwritePermissions(banMember, {
          VIEW_CHANNEL: false,
        });

        // setTimeout(() => {
        //   message.channel.overwritePermissions(banMember, {
        //     VIEW_CHANNEL: null,
        //   });
        //   banMember.user.send(`You have been unbanned`);

        //   return message.channel.send(`${username} is unbanned`);
        // }, ttlValue);

        banMember.user.send(
          `You have been banned for ${args[1]} with a reason: ${args
            .slice(2)
            .join(' ')}`
        );

        return message.channel.send(
          `${username} is banned with a reason: ${banReason}`
        );
      } catch (e) {
        console.error(e.message);
        if (e.message.toLowerCase().indexOf('time frame') >= 0) {
          message.reply(
            'Timeframe is not correct, perhaps 10secs instead of 10 secs ?'
          );
        }
      }
    } else {
      return message.author.send(`username and TTL must be provided`);
    }
  },
};
