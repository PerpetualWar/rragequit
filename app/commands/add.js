const database = require('../../database/database');
const getMember = require('../utils/getMemberObject');
const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const getPlayers = require('../../database/queries/getPlayers');
const getServers = require('../../database/queries/getServers');
const printPlayers = require('../utils/printPlayers');
const formatter = require('../utils/playersFormatter');
const { captainsGenerator, mapPickerGenerator } = require('../utils/rng');
const setTopic = require('../utils/topicSetter');
const customEmbed = require('../utils/embeds/customEmbed');
const pickupReadyEmbed = require('../utils/embeds/pickupReadyEmbed');
const pickupReady = require('../utils/pickupReady');
const dbErrors = require('../../exceptions/dbErrors');
const setAddedPlayers = require('../../database/queries/setAddedPlayers');

module.exports = {
  name: 'add',
  description: 'Add player to designated pickup',
  aliases: ['a'],
  allArguments: ['<pickup name>'],
  adminArgs: false,
  guildOnly: true,
  cooldown: 5,
  async execute(message, args) {
    const {
      id,
      user: { username },
    } = getMember(message, args);

    const guildId = message.guild.id;
    const channelId = message.channel.id;
    const gametypeName = args[0];
    console.log(gametypeName);

    try {
      const player = await getPlayers(id);
      if (player.length === 0)
        return message.reply(' Use `register` command first');

      if (!gametypeName)
        return message.reply(' Provide gametype argument please');

      const gametypes = await getGametypes(channelId);

      let numberOfPlayers;
      let gametypeId;

      gametypes.forEach(gametype => {
        if (gametype.name === gametypeName) {
          numberOfPlayers = gametype.number_of_players;
          gametypeId = gametype.id;
        }
      });
      console.log('message :', message);

      const setPlayers = await setAddedPlayers(
        id,
        guildId,
        channelId,
        gametypeId
      );
      console.log(setPlayers);

      const addedPickupPlayers = await getAddedPlayers(channelId, gametypeId);
      console.log(addedPickupPlayers);
      console.log(numberOfPlayers);

      //when we reach enough players, generate captains and map picker, show server link,
      //delete table entries and send message it is ready
      if (addedPickupPlayers.length === numberOfPlayers) {
        await pickupReady(message, addedPickupPlayers);
      } else {
        setTopic(message.channel);
        printPlayers(message.channel, gametypeId);
      }
    } catch (e) {
      console.error(e);
      dbErrors(e);
      if (
        e.message
          .toLowerCase()
          .indexOf('duplicate key value violates unique constraint') >= 0
      ) {
        console.log('works');
        message.channel.send(username + ' is already added!');
      }
      if (
        e.message
          .toLowerCase()
          .indexOf(
            'null value in column "gametype_id" violates not-null constraint'
          ) >= 0
      ) {
        console.log('works');
        message.channel.send('Gametype does not exist!');
      }
    }
  },
};
