const getPickups = require('../../database/queries/getGametypes');
const embedPickups = require('../utils/embeds/customEmbed');

module.exports = {
  name: 'list_pickups',
  description: 'List all pickups for a given channel',
  aliases: ['lp'],
  allArguments: [],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const pickups = await getPickups();
    const channel = message.channel;

    const channelPickups = pickups.filter(
      pickup => pickup.channel_id === channel.id
    );

    if (channelPickups.length === 0)
      return message.reply('No pickups set for this channel');

    let pickupNames = [];
    channelPickups.forEach(pickup => {
      pickupNames.push(pickup.name);
    });
    console.log('pickupNames :', pickupNames);

    const embed = embedPickups('Registered channel pickups', pickupNames);

    message.channel.send({ embed });
  },
};
