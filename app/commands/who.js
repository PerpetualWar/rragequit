const printPlayers = require('../utils/printPlayers');

module.exports = {
  name: 'who',
  description: 'List of all players added to the pickups',
  aliases: ['w'],
  allArguments: [],
  guildOnly: true,
  cooldown: 5,
  async execute(message, args) {
    try {
      console.log(message);
      await printPlayers(message.channel);
      console.log('test');
    } catch (e) {
      console.error(e);
      return message.channel.send('Nobody is added yet!');
    }
  },
};
