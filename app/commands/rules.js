const getGametypes = require('../../database/queries/getGametypes');
const getRules = require('../../database/queries/getRules');
const printPlayers = require('../utils/printPlayers');
const rulesEmbed = require('../utils/embeds/customEmbed');

module.exports = {
  name: 'rules',
  description: 'Shows rules for a given pickup',
  aliases: ['rl', 'lr'],
  allArguments: [],
  admin: false,
  guildOnly: true,

  async execute(message, args) {
    const guildId = message.guild.id;
    console.log(guildId);

    const rulesArr = await getRules(guildId);

    if (rulesArr.length === 0)
      return message.reply(' there are not rules set for this guild');

    const [{ rules }] = rulesArr;

    const embed = rulesEmbed(`Rules are: ${rules}`);
    message.channel.send({ embed });
  },
};
