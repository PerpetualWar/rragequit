const getMember = require('../utils/getMemberObject');
const timedUserAction = require('../utils/timedUserAction');
const database = require('../../database/database');

module.exports = {
  name: 'unmute',
  description: 'Unmute given player at channel',
  aliases: ['um'],
  allArguments: ['<player name>'],
  admin: true,
  guildOnly: true,
  async execute(message, args) {
    if (args.length > 0) {
      const {
        id,
        user: { username },
      } = getMember(message, args, true);

      try {
        const mutedMember =
          message.channel.members.get(id) ||
          (await message.guild.fetchMember(id));

        message.channel.overwritePermissions(mutedMember, {
          SEND_MESSAGES: null,
        });

        console.log('mutedMember :', mutedMember);

        await database('muted_players')
          .where({
            unmuted_at: null,
            player_id: id,
            channel_id: message.channel.id,
          })
          .update({
            unmuted_at: database.fn.now(),
          });

        mutedMember.user.send(`You have been unmuted`);

        return message.channel.send(`${username} is unmuted`);
      } catch (e) {
        console.error(e);
      }
    } else {
      return message.author.send(`username and TTL must be provided`);
    }
  },
};
