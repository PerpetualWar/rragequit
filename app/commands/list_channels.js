const getChannels = require('../../database/queries/getChannels');
const embedChannels = require('../utils/embeds/customEmbed');

module.exports = {
  name: 'list_channels',
  description: 'List all pickup channels from any guild channel',
  aliases: ['lc'],
  allArguments: [],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const channels = await getChannels();
    const guild = message.guild;

    const guildChannels = channels.filter(
      channel => channel.guild_id === guild.id
    );
    console.log('guildChannels :', guildChannels);

    let channelNames = [];
    guildChannels.forEach(channel => {
      const discordChannel = guild.channels.get(channel.id);
      channelNames.push(discordChannel.name);
    });
    console.log('channelNames :', channelNames);

    const embed = embedChannels('Registered channels', channelNames);

    message.channel.send({ embed });
  },
};
