const getPlayers = require('../../database/queries/getPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const setAddedPlayers = require('../../database/queries/setAddedPlayers');
const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const printPlayers = require('../utils/printPlayers');
const setTopic = require('../utils/topicSetter');
const pickupReady = require('../utils/pickupReady');

module.exports = {
  name: 'push',
  description: 'Push player on designated pickup',
  aliases: [],
  allArguments: ['<player name>', '<pickup name>'],
  admin: true,
  guildOnly: true,

  async execute(message, args) {
    console.log(message);
    console.log(args);
    const playerName = args[0];
    const pickupName = args[1];

    const guildId = message.guild.id;
    const members = message.channel.members;
    console.log(members);
    const channelId = message.channel.id;

    const member = members.find(val => val.user.username === playerName);
    console.log(member);
    const playerId = member.id;

    try {
      const [
        { id: gametypeId, number_of_players: numberOfPlayers },
      ] = await getGametypes(channelId, null, pickupName);

      await setAddedPlayers(playerId, guildId, channelId, gametypeId);

      const players = await getAddedPlayers(channelId, gametypeId);
      console.log(players);

      if (players.length === numberOfPlayers) {
        await pickupReady(message, players);
      } else {
        printPlayers(message.channel, gametypeId);
        setTopic(message.channel);
      }
    } catch (e) {
      if (
        e.message
          .toLowerCase()
          .indexOf('duplicate key value violates unique constraint') >= 0
      ) {
        console.log('works');
        message.channel.send(playerName + ' is already added!');
      }
      if (
        e.message
          .toLowerCase()
          .indexOf(
            'null value in column "gametype_id" violates not-null constraint'
          ) >= 0
      ) {
        console.log('works');
        message.channel.send('Gametype does not exist!');
      }
    }
  },
};
