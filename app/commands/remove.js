const database = require('../../database/database');
const getMember = require('../utils/getMemberObject');
const setTopic = require('../utils/topicSetter');
const printPlayers = require('../utils/printPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const getPlayers = require('../../database/queries/getPlayers');

module.exports = {
  name: 'remove',
  description: 'Remove player from designated pickup',
  aliases: ['r'],
  allArguments: ['<gametype name>'],
  adminArgs: false,
  guildOnly: true,
  cooldown: 5,
  async execute(message, args) {
    const {
      id,
      user: { username },
    } = getMember(message, args);

    const channelId = message.channel.id;
    const gametypeName = args[0];
    console.log('gametypeName :', gametypeName);

    try {
      const player = await getPlayers(id);
      if (player.length === 0)
        return message.reply(' Use `register` command first');

      if (gametypeName) {
        const gametypes = await getGametypes(channelId);
        gametypes.forEach(async gametype => {
          if (gametype.name === gametypeName) {
            const { id: gametypeId } = gametype;
            await database('added_players')
              .where({
                player_id: id,
                channel_id: channelId,
                gametype_id: gametypeId,
              })
              .delete();
          }
        });
        console.log('gametypes :', gametypes);
      } else {
        await database('added_players')
          .where({ player_id: id, channel_id: channelId })
          .delete();
      }

      setTopic(message.channel);
      message.channel.send(printPlayers(message.channel));
    } catch (e) {
      console.error(e);
      return message.channel.send(username + ' is not removed!');
    }
  },
};
