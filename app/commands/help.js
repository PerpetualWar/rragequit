const helpEmbed = require('../utils/embeds/helpEmbed');
const { isUserAdmin } = require('../utils/admin');

module.exports = {
  name: 'help',
  description: 'Help about bot commands',
  aliases: ['h'],
  allArguments: ['<command name>'],
  admin: false,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);
    const command = args[0];
    const commands = message.client.commands;
    const userAdmin = await isUserAdmin(message);

    const nonAdminCommands = commands.filter(value => !value.admin);

    let embed;
    if (command) {
      userAdmin
        ? (embed = helpEmbed(true, commands, command))
        : (embed = helpEmbed(true, nonAdminCommands, command));
    } else {
      userAdmin
        ? (embed = helpEmbed(false, commands))
        : (embed = helpEmbed(false, nonAdminCommands));
    }

    if (embed) message.channel.send({ embed });
    else message.reply(` command doesn't exist for your permission scope`);
  },
};
