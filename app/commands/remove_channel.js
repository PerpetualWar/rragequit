const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setChannel = require('../../database/queries/setChannel');
const unsetChannel = require('../../database/queries/unsetChannel');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'remove_channel',
  description: 'Unregisters pickup channel',
  aliases: ['rc'],
  allArguments: ['<channel name>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    if (args.length > 0) {
      const channelName = args[0];
      const guildId = message.guild.id;
      const channel = message.guild.channels.find(
        channel => channel.name === channelName
      );

      if (!channel) return message.reply('Provide existing db channel name');

      try {
        await unsetChannel(guildId, channel);
        message.reply(`Channel is removed from db`);
      } catch (e) {
        console.error(e.message);
      }
    } else return message.reply(`You must provide channel name`);
  },
};
