const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setPlayers = require('../../database/queries/setPlayers');
const setGametype = require('../../database/queries/setGametype');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'set_prefix',
  description: 'Sets prefix for bot commands',
  aliases: [],
  allArguments: ['<prefix>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const prefix = args[0];
    const guildId = message.guild.id;
    console.log(prefix);
    console.log(guildId);

    if (args.length > 0) {
      await setPrefix(guildId, prefix);
      return message.reply(`Prefix is set`);
    } else return message.reply(`You must provide command and value`);
  },
};
