const getPlayers = require('../../database/queries/getPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const setGametype = require('../../database/queries/setGametype');
const removeAddedPlayers = require('../../database/queries/removeAddedPlayers');
const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const printPlayers = require('../utils/printPlayers');
const setTopic = require('../utils/topicSetter');
const setMap = require('../../database/queries/setMap');

module.exports = {
  name: 'set_maps',
  description: 'Add or edit maps for a given pickup',
  aliases: [],
  allArguments: ['<gametype>', '<map_name(1)>,<map_name(2)>,...<map_name(n)>'],
  admin: true,
  guildOnly: true,

  async execute(message, args) {
    console.log(message);
    console.log(args);
    const [gametype, ...maps] = args;
    console.log(maps);
    const mapsString = maps.join(',');
    console.log(mapsString);
    channelId = message.channel.id;

    const [{ id: gametypeId }] = await getGametypes(channelId, null, gametype);

    // const gametypes23 = await getGametypes(
    //   channelId,
    //   null,
    //   gametype
    // );
    // const gametype = gametypes[0];
    // const id = gametype.id;

    // console.log('gametypoes', gametypes23)

    try {
      await setMap(mapsString, gametypeId);
      message.reply('Maps are set');
    } catch (e) {
      message.reply('error with saving to db');
    }
  },
};
