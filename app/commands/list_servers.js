const getServers = require('../../database/queries/getServers');
const embedServers = require('../utils/embeds/customEmbed');

module.exports = {
  name: 'list_servers',
  description: 'List all registered game servers',
  aliases: ['ls'],
  allArguments: [],
  admin: false,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const servers = await getServers();
    console.log('servers :', servers);
    let serversOutput = [];
    servers.forEach(server => {
      serversOutput.push(server.name);
    });

    const embed = embedServers('Available servers', serversOutput);

    message.channel.send({ embed });
  },
};
