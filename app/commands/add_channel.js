const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setChannel = require('../../database/queries/setChannel');
const getChannels = require('../../database/queries/getChannels');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'add_channel',
  description: 'Registers channel for pickup games',
  aliases: ['ac'],
  allArguments: ['<channel name>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const channels = await getChannels();

    if (args.length > 0) {
      const channelName = args[0];
      const guildId = message.guild.id;
      const channel = message.guild.channels.find(
        channel => channel.name === channelName
      );
      if (channels.some(item => item.id === channel.id))
        return message.reply('Channel already added!');

      if (!channel) return message.reply('Provide existing channel name');

      try {
        await setChannel(guildId, channel);
        message.reply(`Channel is set`);
      } catch (e) {
        console.error(e.message);
      }
    } else return message.reply(`You must provide channel name`);
  },
};
