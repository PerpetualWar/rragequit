const getGametypes = require('../../database/queries/getGametypes');
const getMaps = require('../../database/queries/getMaps');
const printPlayers = require('../utils/printPlayers');
const mapsEmbed = require('../utils/embeds/customEmbed');

module.exports = {
  name: 'maps',
  description: 'Shows maps for a given pickup',
  aliases: ['mp', 'mps'],
  allArguments: ['<gametypeName>'],
  admin: false,
  guildOnly: true,

  async execute(message, args) {
    console.log(message);
    const gametypeName = args[0];
    console.log(args);
    channelId = message.channel.id;
    const gametype = await getGametypes(channelId, null, gametypeName);

    if (gametype.length === 0)
      return message.reply(' please provide an existing pickup name');

    const [{ id: gametypeId }] = gametype;

    const mapsArray = await getMaps(gametypeId);
    console.log(mapsArray);
    if (!gametypeName)
      return message.reply(' please provide a pickup name for map listing');
    if (mapsArray.length === 0) {
      return message.reply(
        ` there are not defined maps for ${gametypeName} pickup`
      );
    }
    const gametypeMaps = maps.split(',');
    console.log(gametypeMaps);
    const embed = mapsEmbed(`${gametypeName} pickup maps`, gametypeMaps);
    message.channel.send({ embed });
  },
};
