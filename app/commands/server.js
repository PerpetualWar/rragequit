// const setChannels = require('../../database/queries/setChannel');
// const setPrefix = require('../../database/queries/setPrefix');
// const setAdmin = require('../../database/queries/setAdmin');
// const setServer = require('../../database/queries/setServer');
const getGametypes = require('../../database/queries/getGametypes');
// const setTopic = require('../utils/topicSetter');
const updateServer = require('../../database/queries/updateServer');
const getServers = require('../../database/queries/getServers');

module.exports = {
  name: 'server',
  description: 'Accepts user provided server for next match ',
  aliases: ['s'],
  allArguments: ['<server name>', '<pickup name>'],
  admin: false,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);
    const guildId = message.guild.id;
    const channelId = message.channel.id;
    const serverName = args[0];
    const gametypeName = args[1];
    let gametypeId;

    const server = await getServers(serverName, gametypeId, guildId);
    if (serverName && gametypeName) {
      console.log('serverName :', serverName);
      console.log('gametypeName :', gametypeName);
      try {
        const gametypes = await getGametypes(channelId);
        gametypes.forEach(gametype => {
          if (gametype.name === gametypeName) {
            gametypeId = gametype.id;
          }
        });
        console.log('gametypes :', gametypes);
        console.log('server :', server);

        if (server) {
          await updateServer(server.id, true);
        }
      } catch (e) {
        console.error(e);
      }
    } else message.reply(' please provide server name and gametype name');
  },
};
