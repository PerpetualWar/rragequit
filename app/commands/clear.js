const database = require('../../database/database');
const printPlayers = require('../utils/printPlayers');
const setTopic = require('../utils/topicSetter');
const getGametypes = require('../../database/queries/getGametypes');

module.exports = {
  name: 'clear',
  description: 'Remove all players from pickup',
  allArguments: ['<pickup name>'],
  aliases: [],
  admin: true,
  guildOnly: true,
  async execute(message, args) {
    const channelId = message.channel.id;
    const gametypeName = args[0];
    console.log('args :', args);
    console.log('gametypeName :', gametypeName);
    try {
      if (!gametypeName)
        return message.channel.send('Please provide the gametype argument');

      const gametypes = await getGametypes(channelId);
      gametypes.forEach(async gametype => {
        if (gametype.name === gametypeName) {
          const { id: gametypeId } = gametype;
          await database('added_players')
            .where({ gametype_id: gametypeId })
            .delete();
        }
      });
      setTopic(message.channel);
      // message.channel.send(printPlayers(message.channel));
    } catch (e) {
      console.error(e);
      return message.channel.send('Bad request!');
    }
  },
};
