const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setServer = require('../../database/queries/setServer');
const getGametypes = require('../../database/queries/getGametypes');
const setTopic = require('../utils/topicSetter');
const unsetServer = require('../../database/queries/unsetServer');

module.exports = {
  name: 'remove_server',
  description: 'Remove registered game server from database',
  aliases: ['rs'],
  allArguments: ['<server name>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    if (args.length > 0) {
      const [serverName] = args;
      const guildId = message.guild.id;

      await unsetServer(serverName, guildId);
      message.reply(' server removed from db');
    } else message.reply(' please provide server name');
  },
};
