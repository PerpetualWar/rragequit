const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setGametype = require('../../database/queries/setGametype');
const getGametypes = require('../../database/queries/getGametypes');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'add_pickup',
  description: 'Add pickup directly from channel',
  aliases: ['ap'],
  allArguments: ['<pickup name>', '<number of players>', '<auto remove limit>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    const pickups = await getGametypes();

    if (args.length > 1) {
      const pickupName = args[0];
      const numberOfPlayers = args[1];
      const autoRemoveLimit = args[2];
      const channelId = message.channel.id;
      const guildId = message.guild.id;
      const values = [pickupName, numberOfPlayers, channelId, autoRemoveLimit];

      if (
        pickups.some(
          item => item.name === pickupName && item.channel_id === channelId
        )
      )
        return message.reply('Pickup already exist!');

      try {
        console.log('values :', values);
        const channel = message.client.channels.get(channelId);
        await setGametype(guildId, values);
        setTopic(channel);
        message.reply(`Pickup is set`);
      } catch (e) {
        console.error(e.message);
        // message.author.send(e.message);
      }
    } else
      return message.reply(
        `You must provide name and number of players for the pickup`
      );
  },
};
