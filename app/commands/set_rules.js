const setRules = require('../../database/queries/setRules');
const printPlayers = require('../utils/printPlayers');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'set_rules',
  description: 'Sets rules for a guild',
  aliases: [],
  allArguments: ['<link to rules>'],
  admin: true,
  guildOnly: true,

  async execute(message, args) {
    const guildId = message.guild.id;
    const rules = args[0];

    try {
      await setRules(rules, guildId);
      message.reply(' rules are set');
    } catch (e) {
      message.reply('Error with saving to db');
    }
  },
};
