const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const setServer = require('../../database/queries/setServer');
const getGametypes = require('../../database/queries/getGametypes');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'add_server',
  description: 'Add game server to the database',
  aliases: ['as'],
  allArguments: ['<server ip>', '<server name>', '<server password>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    if (args.length > 2) {
      const [ip, name, password] = args;

      const guildId = message.guild.id;
      const channelId = message.channel.id;

      // const [{ id: gametypeId }] = await getGametypes(
      //   channelId,
      //   null,
      //   gametypeName
      // );

      await setServer(ip, name, password, guildId);
      message.reply(' Server set!');
    } else message.reply('Please provide server ip , name and password');
  },
};
