const database = require('../../database/database');
const getMember = require('../utils/getMemberObject');
const insert = require('../../database/queries/insert');
const setPlayers = require('../../database/queries/setPlayers');

module.exports = {
  name: 'register',
  description: 'Register player with bot',
  aliases: ['reg'],
  allArguments: [],
  adminArgs: false,
  admin: false,
  guildOnly: true,
  cooldown: 5,
  async execute(message, args) {
    const {
      id,
      user: { username },
    } = getMember(message, args);

    console.log(id, username);

    try {
      await setPlayers(id);
      // await database('players')
      //   .insert({
      //     id,
      //     username,
      //   })
      //   .returning('id');

      message.channel.send(`${username} is now registered!`);
      return id;
    } catch (e) {
      console.error(e);
      return message.channel.send(username + ' is already registered!');
    }
  },
};
