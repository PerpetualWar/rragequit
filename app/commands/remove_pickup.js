const setChannels = require('../../database/queries/setChannel');
const setPrefix = require('../../database/queries/setPrefix');
const setAdmin = require('../../database/queries/setAdmin');
const unsetGametype = require('../../database/queries/unsetGametype');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'remove_pickup',
  description: 'Registers new pickup for given channel',
  aliases: ['rp'],
  allArguments: ['<pickup name>'],
  admin: true,
  guildOnly: true,
  dmOnly: false,
  async execute(message, args) {
    console.log('args:', args);
    console.log('message :', message);

    if (args.length > 0) {
      const pickupName = args[0];
      const channelId = message.channel.id;

      try {
        const channel = message.client.channels.get(channelId);
        await unsetGametype(pickupName, channelId);
        setTopic(channel);
        message.reply(`Pickup is removed`);
      } catch (e) {
        console.error(e.message);
      }
    } else message.reply('You must provide name of the pickup to be removed');
  },
};
