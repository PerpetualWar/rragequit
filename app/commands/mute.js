const getMember = require('../utils/getMemberObject');
const ttl = require('../utils/timedUserAction');
const ttlInsert = require('../../database/queries/ttlInsert');
const database = require('../../database/database');

module.exports = {
  name: 'mute',
  description: 'Mute player at a given channel',
  aliases: ['m'],
  allArguments: ['<player name>', '<mute timeframe>', '<mute reason>'],
  admin: true,
  guildOnly: true,
  async execute(message, args) {
    if (args.length > 1) {
      const {
        id,
        user: { username },
      } = getMember(message, args, true);

      const muteReason = args.slice(2).join(' ');
      const channelId = message.channel.id;
      const guildId = message.guild.id;

      try {
        //first check if player already have an active mute
        const mutedPlayer = await database('muted_players')
          .where({
            unmuted_at: null,
            player_id: id,
            channel_id: channelId,
          })
          .select();

        //if active mute, just exit early and inform user
        if (mutedPlayer.length > 0)
          return message.reply(' player is already muted');

        console.log('mutedPlayer :', mutedPlayer);

        const muteMember =
          message.channel.members.get(id) ||
          (await message.guild.fetchMember(id));

        const ttlValue = ttl(args[1]);
        console.log(ttlValue);

        await ttlInsert('muted', id, channelId, guildId, ttlValue, muteReason);

        await message.channel.overwritePermissions(muteMember, {
          SEND_MESSAGES: false,
        });

        muteMember.user.send(
          `You have been muted for ${args[1]} with a reason: ${args
            .slice(2)
            .join(' ')}`
        );

        return message.channel.send(
          `${username} is muted with a reason: ${muteReason}`
        );
      } catch (e) {
        console.error(e.message);
        if (e.message.toLowerCase().indexOf('time frame') >= 0) {
          message.reply(
            'Timeframe is not correct, perhaps 10secs instead of 10 secs ?'
          );
        }
      }
    } else {
      return message.author.send(`username and TTL must be provided`);
    }
  },
};
