const getMember = require('../utils/getMemberObject');
const timedUserAction = require('../utils/timedUserAction');
const database = require('../../database/database');

module.exports = {
  name: 'unban',
  description: 'Unban given player at channel',
  aliases: ['ub'],
  allArguments: ['<player name>'],
  admin: true,
  guildOnly: true,
  async execute(message, args) {
    if (args.length > 0) {
      const {
        id,
        user: { username },
      } = getMember(message, args, true);

      // const member = guild.member(user) || (await guild.fetchMember(user));

      try {
        const bannedMember =
          message.channel.members.get(id) ||
          (await message.guild.fetchMember(id));

        message.channel.overwritePermissions(bannedMember, {
          VIEW_CHANNEL: null,
        });

        console.log('bannedMember :', bannedMember);

        await database('banned_players')
          .where({
            unbanned_at: null,
            player_id: id,
            channel_id: message.channel.id,
          })
          .update({
            unbanned_at: database.fn.now(),
          });

        bannedMember.user.send(`You have been unbanned`);

        return message.channel.send(`${username} is unbanned`);
      } catch (e) {
        console.error(e);
      }
    } else {
      return message.author.send(`username and TTL must be provided`);
    }
  },
};
