const getPlayers = require('../../database/queries/getPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const removeAddedPlayers = require('../../database/queries/removeAddedPlayers');
const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const printPlayers = require('../utils/printPlayers');
const setTopic = require('../utils/topicSetter');

module.exports = {
  name: 'pull',
  description: 'Pull player from designated pickup',
  aliases: [],
  allArguments: ['<player name>', '<pickup name>'],
  admin: true,
  guildOnly: true,

  async execute(message, args) {
    console.log(message);
    console.log(args);
    const playerName = args[0];
    const pickupName = args[1];

    const members = message.channel.members;
    console.log(members);
    const member = members.find(val => val.user.username === playerName);
    console.log(member);
    const playerId = member.id;
    console.log(member.id);
    const channelId = message.channel.id;

    const [{ id: gametypeId }] = await getGametypes(
      channelId,
      null,
      pickupName
    );

    const players = await getAddedPlayers(channelId, gametypeId);
    console.log(players);

    if (players.length === 0) {
      return message.reply(
        'Player with the name ' + playerName + ' is not added to the pickup'
      );
    }

    for (const player of players) {
      console.log(player);
      if (playerId !== player.player_id) {
        console.log('it runs');
        return message.reply(
          'Player with the name ' + playerName + ' is not added to the pickup'
        );
      }
    }

    await removeAddedPlayers(playerId, gametypeId);
    printPlayers(message.channel, gametypeId);
    setTopic(message.channel);
  },
};
