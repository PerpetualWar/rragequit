const isPast = require('date-fns/is_past');
// const getBannedPlayers = require('../../database/queries/getBannedPlayers');
const getSanctionedPlayers = require('../../database/queries/getSanctionedPlayers');
const getAddedPlayers = require('../../database/queries/joins/gametypes_addedplayers');
const database = require('../../database/database');
const setTopic = require('../utils/topicSetter');
const printPlayers = require('../utils/printPlayers');

/**
 *
 *
 * @param {Object} client
 */
module.exports = async client => {
  const [bannedPlayers, mutedPlayers, players] = await Promise.all([
    getSanctionedPlayers('banned'),
    getSanctionedPlayers('muted'),
    getAddedPlayers(),
  ]);
  console.log('bannedPlayers :', bannedPlayers);
  console.log('mutedPlayers :', mutedPlayers);
  console.log('players :', players);
  console.log('client :', client);

  //auto remove players polling
  players.forEach(async player => {
    const autoRemoveLimit = Number(player.auto_remove_limit);
    const channelId = player.channel_id;
    const gametypeId = player.gametype_id;
    const playerId = player.player_id;

    if (autoRemoveLimit) {
      const date = new Date(player.created_at);
      const dateInMillis = date.getTime(date);

      const finalInMillis = dateInMillis + autoRemoveLimit;
      console.log('date :', finalInMillis);
      const autoRemovedAfter = new Date(finalInMillis).toISOString();
      console.log(autoRemovedAfter);

      const result = isPast(autoRemovedAfter);
      console.log('result :', result);

      if (result) {
        await database('added_players')
          .where({ gametype_id: gametypeId, player_id: playerId })
          .del();
        const channel = client.channels.get(channelId);
        console.log('channel', channel);
        setTopic(channel);
        printPlayers(channel, gametypeId);
      }
    }
  });

  //banned players polling
  bannedPlayers.forEach(async item => {
    const bannedFor = Number(item.banned_for);
    const bannedAt = item.created_at;
    const id = item.id;
    const playerId = item.player_id;
    const channelId = item.channel_id;
    const guildId = item.guild_id;
    const unbannedAt = item.unbanned_at;

    console.log('unbannedAt :', unbannedAt);

    const guild = client.guilds.get(guildId);

    const channel = client.channels.get(channelId);
    console.log(channel);
    const bannedMember =
      channel.members.get(playerId) || (await guild.fetchMember(playerId));
    console.log(bannedMember);

    const date = new Date(bannedAt);
    const bannedAtInMillis = date.getTime();

    const finalInMillis = bannedAtInMillis + bannedFor;
    const bannedUntil = new Date(finalInMillis).toISOString();
    console.log(bannedUntil);

    const result = isPast(bannedUntil);
    console.log('result :', result);
    if (result && !unbannedAt) {
      channel.overwritePermissions(bannedMember, {
        VIEW_CHANNEL: null,
      });

      await database('banned_players')
        .where({
          id,
          player_id: playerId,
          channel_id: channelId,
        })
        .update({
          unbanned_at: database.fn.now(),
        });

      bannedMember.user.send(`You have been unbanned`);

      channel.send(`${bannedMember.user.username} is unbanned`);
    }
  });

  //muted players polling
  mutedPlayers.forEach(async item => {
    const mutedFor = Number(item.muted_for);
    const mutedAt = item.created_at;
    const id = item.id;
    const playerId = item.player_id;
    const channelId = item.channel_id;
    const guildId = item.guild_id;
    const unmutedAt = item.unmuted_at;

    console.log('unmutedAt :', unmutedAt);

    const guild = client.guilds.get(guildId);

    const channel = client.channels.get(channelId);
    console.log(channel);
    const mutedMember =
      channel.members.get(playerId) || (await guild.fetchMember(playerId));
    console.log(mutedMember);

    const date = new Date(mutedAt);
    const mutedAtInMillis = date.getTime();

    const finalInMillis = mutedAtInMillis + mutedFor;
    const mutedUntil = new Date(finalInMillis).toISOString();
    console.log(mutedUntil);

    const result = isPast(mutedUntil);
    console.log('result :', result);
    if (result && !unmutedAt) {
      channel.overwritePermissions(mutedMember, {
        SEND_MESSAGES: null,
      });

      await database('muted_players')
        .where({
          id,
          player_id: playerId,
          channel_id: channelId,
        })
        .update({
          unmuted_at: database.fn.now(),
        });

      mutedMember.user.send(`You have been unmuted`);

      channel.send(`${mutedMember.user.username} is unmuted`);
    }
  });
};
