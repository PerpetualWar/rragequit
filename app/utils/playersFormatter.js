module.exports = (players, message) => {
  console.log('players :', players);
  console.log('message :', message);

  return `
  [ ${players
    .map(player => {
      return message.channel.members.get(player.player_id);
    })
    .join(' || ')} ]
  `;
};
