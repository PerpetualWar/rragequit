// const embed = require('./embed');
// const getGametypes = require('../../database/queries/getGametypes');

module.exports = (discordPlayers, dbPlayers, channelGametypes) => {
  let formattedPlayers = [];
  let formattedGametypes = [];
  let output = ``;
  // let output = `======================= Added Players ======================= `;
  let formattedOutput = ``;

  console.log('discordPlayers :', discordPlayers);
  console.log('dbPlayers :', dbPlayers);
  console.log('channelGametypes :', channelGametypes);
  let name, id, username;
  // if (channelGametypes && channelGametypes.length > 0) {
  for (const gametype of channelGametypes) {
    name = gametype.name;
    id = gametype.id;

    formattedGametypes.push(name);

    console.log('dbPlayers :', dbPlayers);
    if (dbPlayers.length > 0) {
      dbPlayers.forEach(addedPlayer => {
        if (addedPlayer.gametype_id === id) {
          discordPlayers.forEach(player => {
            if (player.user.id === addedPlayer.player_id) {
              username = player.user.username;
            }
          });
          console.log('dbPlayers :', dbPlayers);
          formattedPlayers.push([name, [username]]);
        } else {
          formattedPlayers.push([name, []]);
        }
      });
    } else {
      formattedPlayers.push([name, []]);
    }
  }

  //make unique entries
  formattedPlayers = [
    ...new Set(
      formattedPlayers.filter(formattedPlayer => formattedPlayer[1].length)
    ),
  ];

  console.log('formattedPlayers :', formattedPlayers);
  console.log('formattedGametypes :', formattedGametypes);

  //finally generate output string
  formattedGametypes.forEach(formattedGametype => {
    output += `
    **${formattedGametype}**: [ ${formattedPlayers
      .map(formattedPlayer => {
        if (formattedGametype === formattedPlayer[0]) {
          return formattedPlayer[1];
        }
      })
      .filter(item => item)
      .join(' || ')} ]`;
  });
  console.log('formattedOutput :', formattedOutput);
  console.log('output :', output);
  return output;
};
