const sampleSize = require('lodash/sampleSize');

exports.captainsGenerator = (players, message) => {
  const res = sampleSize(players, 2);
  console.log('res :', res);

  let { player_id: cap1 } = res[0];
  let { player_id: cap2 } = res[1];

  console.log(message);
  cap1 = message.channel.members.get(cap1);
  cap2 = message.channel.members.get(cap2);

  console.log('cap1 :', cap1);

  return `Captains are: ${cap1} and ${cap2}`;
};

exports.mapPickerGenerator = (players, message) => {
  const res = sampleSize(players, 1);
  console.log('res :', res);
  let { player_id: mapPickerId } = res[0];

  console.log(message);
  mapPicker = message.channel.members.get(mapPickerId);

  return `Map picker is: ${mapPicker}`;
};
