const distanceInWordsStrict = require('date-fns/distance_in_words_strict');

module.exports = addedPlayer => {
  const addedAt = addedPlayer.created_at;
  const addedAtMillis = new Date(addedAt).getTime();
  const currentTimeMillis = new Date().getTime();

  // const finalInMillis = currentTimeMillis - addedAtMillis;

  const passedTime = distanceInWordsStrict(addedAtMillis, currentTimeMillis, {
    unit: 'm',
  });
  console.log('passedTime :', passedTime);

  return `\`${passedTime.split(' ')[0]} min\``;
};
