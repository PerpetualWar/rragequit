const timePassed = require('../timePassed');

module.exports = (discordPlayers, dbPlayers, channelGametypes) => {
  let fieldsArray = [];
  let pickupPlayers = [];

  //first we create fields array with gametype names and placeholder text
  channelGametypes.forEach(gametype => {
    fieldsArray.push({ name: gametype.name, value: '`<empty>`' });
  });

  //now we need to form players for each gametype
  let name, id, playerObject;

  channelGametypes.forEach(gametype => {
    name = gametype.name;
    id = gametype.id;

    if (dbPlayers.length > 0) {
      dbPlayers.forEach(addedPlayer => {
        const passedTime = timePassed(addedPlayer);

        if (addedPlayer.gametype_id === id) {
          discordPlayers.forEach(player => {
            if (player.user.id === addedPlayer.player_id) {
              playerObject = player;
            }
          });
          console.log('dbPlayers :', dbPlayers);
          pickupPlayers.push([name, [playerObject, passedTime]]);
        } else {
          pickupPlayers.push([name, []]);
        }
      });
    } else {
      pickupPlayers.push([name, []]);
    }
  });

  //make unique entries
  pickupPlayers = [
    ...new Set(pickupPlayers.filter(pickupPlayer => pickupPlayer[1].length)),
  ];

  console.log('pickupPlayers :', pickupPlayers);

  //finally generate output string
  fieldsArray.forEach(field => {
    field.value = ``;
    field.value += `${pickupPlayers
      .map(pickupPlayer => {
        if (field.name === pickupPlayer[0]) {
          return pickupPlayer[1];
        }
      })
      .filter(item => item)
      .join('||')}`;
    if (!field.value) field.value = '`<empty>`';
  });

  console.log('fieldsArray :', fieldsArray);
  console.log('fieldsArray :', fieldsArray);

  return {
    color: 0xff0000,
    title: '**Added players**',
    fields: fieldsArray,
    footer: {
      text: 'Rragequit',
      icon_url:
        'http://i226.photobucket.com/albums/dd281/dj_srle/Rragequit/Red%20with%20transparent%20BG-01-01.png',
    },
  };
};
