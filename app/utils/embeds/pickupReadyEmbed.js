// const capGenerator = require('../rng');
const database = require('../../../database/database');

module.exports = async (pickupId, payload, captains, mapPicker, serverLink) => {
  let output = '';
  const payloadLength = payload.length;
  payload.forEach((item, index) => {
    output += `${item} ${index === payloadLength - 1 ? '' : '||'} `;
    console.log('playerObject :', item);
  });

  const [{ name }] = await database('gametypes')
    .where({ id: pickupId })
    .select('name');

  console.log('name :', name);

  return {
    color: 0xff0000,
    title: `**Pickup ready!**`,
    // description: payload ? output : formatted,
    fields: [
      {
        name: 'Pickup name',
        value: name,
      },
      {
        name: 'Captains',
        value: captains,
      },
      {
        name: 'Players',
        value: output,
      },
      {
        name: 'Map picker',
        value: mapPicker,
      },
    ],
    timestamp: new Date(),
    footer: {
      text: 'Rragequit',
      icon_url:
        'http://i226.photobucket.com/albums/dd281/dj_srle/Rragequit/Red%20with%20transparent%20BG-01-01.png',
    },
  };
};
