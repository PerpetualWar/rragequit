module.exports = (helpWithCommand, commands, command) => {
  if (helpWithCommand) {
    //if command does not exist, exit early
    if (!commands.get(command)) return;

    var commandObj = commands.get(command);
    console.log('commandObj :', commandObj);
    var aliases = commandObj.aliases.join(' ,');
    if (!aliases) aliases = '*none*';

    var { allArguments, allArguments, description } = commandObj;
  }

  //here we spread map iterator to an array
  const keys = [...commands.keys()];

  if (!helpWithCommand)
    return {
      color: 0xff0000,
      title: 'Commands',
      description: `
      ${keys.map(key => '**' + key + '**').join('\n')}
      `,
      footer: {
        text: 'Rragequit',
        icon_url:
          'http://i226.photobucket.com/albums/dd281/dj_srle/Rragequit/Red%20with%20transparent%20BG-01-01.png',
      },
    };

  return {
    color: 0xff0000,
    fields: [
      {
        name: '**Command**',
        value: command,
      },
      {
        name: '**Aliases**',
        value: aliases,
      },
      {
        name: '**Description**',
        value: description,
      },
      {
        name: '**Usage**',
        value: `!${command} ${allArguments.join(' ')}`,
      },
    ],
    footer: {
      text: 'Rragequit',
      icon_url:
        'http://i226.photobucket.com/albums/dd281/dj_srle/Rragequit/Red%20with%20transparent%20BG-01-01.png',
    },
  };
};
