module.exports = (title, payload, formatted) => {
  let output = '';
  if (payload) {
    const payloadLength = payload.length;
    payload.forEach((item, index) => {
      output += `**\`${item}\`** ${index === payloadLength - 1 ? '' : '||'} `;
    });
  }
  return {
    color: 0xff0000,
    title,
    description: payload ? output : formatted,
    footer: {
      text: 'Rragequit',
      icon_url:
        'http://i226.photobucket.com/albums/dd281/dj_srle/Rragequit/Red%20with%20transparent%20BG-01-01.png',
    },
  };
};
