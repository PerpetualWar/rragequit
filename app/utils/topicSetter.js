const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const getChannels = require('../../database/queries/getChannels');
const formatter = require('./gametypesFormatter');
const database = require('../../database/database');

module.exports = async channel => {
  const addedPlayers = await getAddedPlayers(channel.id);
  const gametypes = await getGametypes(channel.id);
  // console.log('players :', players);

  let topicString = '';
  const gametypeLength = gametypes.length;
  gametypes.forEach((gametype, index) => {
    let currentPlayers = 0;
    const { name, number_of_players: numberOfPlayers } = gametype;
    if (addedPlayers && addedPlayers.length > 0) {
      addedPlayers.forEach(player => {
        if (player.gametype_id === gametype.id) {
          currentPlayers += 1;
          console.log(currentPlayers);
        }
      });
    }
    topicString += `**${name}**: [${currentPlayers}/${numberOfPlayers}]  ${
      index === gametypeLength - 1 ? '' : '||'
    }  `;
  });

  console.log('topicString :', topicString);
  channel.setTopic(topicString);
};
