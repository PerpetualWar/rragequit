const database = require('../../database/database');
const getServers = require('../../database/queries/getServers');
const { captainsGenerator, mapPickerGenerator } = require('../utils/rng');
const setTopic = require('../utils/topicSetter');
const pickupReadyEmbed = require('../utils/embeds/pickupReadyEmbed');

module.exports = async (message, addedPickupPlayers) => {
  let playerIds = [];
  let players = [];
  let pickupId;
  addedPickupPlayers.forEach(player => {
    pickupId = player.gametype_id;
    playerIds.push(player.player_id);
    const currentPlayer = message.guild.members.get(player.player_id);
    players.push(currentPlayer);
  });

  //generate captains and map picker
  const captains = captainsGenerator(addedPickupPlayers, message);
  const mapPicker = mapPickerGenerator(addedPickupPlayers, message);
  const servers = await getServers();

  console.log('servers :', servers);

  //get embed ready
  const embed = await pickupReadyEmbed(pickupId, players, captains, mapPicker);

  //send message to each player
  players.forEach(player => {
    player.send({ embed });
  });

  //and send global channel message
  message.channel.send({ embed });

  // here we need to remove players from other pickups as well
  await database('added_players')
    .whereIn('player_id', playerIds)
    .delete();

  setTopic(message.channel);
};
