const { isUserAdmin } = require('./../utils/admin');

module.exports = async (command, message, args) => {
  console.log('args :', args);
  console.log('command :', command);
  try {
    //we are testing different use cases based on admin commands, or
    //on admin arguments only, if no restriction on command, execute
    //normally

    const userAdmin = await isUserAdmin(message, args);

    if (command.admin) {
      if (userAdmin) {
        console.log('admin, admin');
        command.execute(message, args);
      } else if (!userAdmin) {
        console.log('admin, not admin');
        message.reply('You are not authorized to run that command');
      }
    } else if (command.adminArgs) {
      if (args.length > 0 && !userAdmin) {
        console.log('adminArgs, not admin');
        message.reply(
          'You are not authorized to provide arguments to this command'
        );
      } else if (args.length === 0 && !userAdmin) {
        console.log('adminArgs, no args');
        command.execute(message, []);
      } else command.execute(message, args);
    } else command.execute(message, args);
  } catch (error) {
    console.error(error);
    message.reply('there was an error trying to execute that command!');
  }
};
