const getGuilds = require('../../database/queries/getGuilds');

exports.getAdminRoleId = message => {
  const { id: adminRoleId } = message.guild.roles.find(
    role => role.name === adminRole
  );
  return adminRoleId;
};

exports.isUserAdmin = async (message, args) => {
  console.log(message);
  console.log(args);

  //normal usecase, when sending message from channel
  if (message.channel.type === 'text') {
    var [{ admin_role: adminRole }] = await getGuilds(message.guild.id);
    console.log('adminRole :', adminRole);
    const res = message.member.roles.some(role => role.name === adminRole);
    console.log(res);
    return res;

    //certain usecase, when sending message to bot's DM
    //usually for specific admin commands
  } else if (message.channel.type === 'dm' && args.length > 0) {
    const guildId = args[0];
    const userId = message.author.id;
    const guild = message.client.guilds.get(guildId);
    const member = guild.members.get(userId);
    const res = member.roles.some(role => role.name === adminRole);
    return res;
  }

  return false;
};
