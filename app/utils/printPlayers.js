const getAddedPlayers = require('../../database/queries/getAddedPlayers');
const getGametypes = require('../../database/queries/getGametypes');
const getGametypesAddedPlayers = require('../../database/queries/getGametypesAddedPlayers');
const formatter = require('./gametypesFormatter');
const addedPlayersEmbed = require('../utils/embeds/addedPlayersEmbed');

module.exports = async (channel, gametypeId) => {
  const [channelGametypes, players] = await Promise.all([
    getGametypes(channel.id, gametypeId),
    getAddedPlayers(channel.id),
  ]);
  // const gametypesPlayers = await getGametypesAddedPlayers(channel.id);

  let discordPlayers = [];
  players.forEach(async player => {
    const playerObj =
      channel.members.get(player.player_id) ||
      (await channel.guild.fetchMember(player.player_id));
    discordPlayers.push(playerObj);
  });

  // const formatted = formatter(prepPlayers, players, channelGametypes);
  const embed = addedPlayersEmbed(discordPlayers, players, channelGametypes);

  // return channel.send(formatted);
  return channel.send({ embed });
};
