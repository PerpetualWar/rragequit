module.exports = (message, args, bool) => {
  //we expose member object either by args username or by author username
  let memberName;
  if (!bool) {
    //used for add, remove, etc.
    //where command is always first , playername second
    memberName = args[1] || null;
  } else {
    //used for ban where name must be first
    memberName = args[0] || null;
  }

  let member;

  if (memberName) {
    member = message.guild.members.find(
      item => item.user.username === memberName
    );

    if (!member)
      return message.channel.send(`No user with that name on server`);
  } else {
    member = message.guild.members.get(message.author.id);
  }

  return member;
};
