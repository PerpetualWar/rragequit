module.exports = [
  'add_channel',
  'remove_channel',
  'list_channels',
  'set_prefix',
  'set_admin',
  'help',
];
