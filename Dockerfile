# Dockerfile

FROM node:10
COPY . .

# RUN git clone https://github.com/vishnubob/wait-for-it.git
RUN apt-get update && apt-get install nano
RUN yarn add global gulp-cli && yarn add global pm2 && yarn build
WORKDIR /dist
RUN yarn install
# CMD node index.js
CMD ["yarn", "start"]
EXPOSE 443
