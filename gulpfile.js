const gulp = require('gulp');
const stripDebug = require('gulp-strip-debug');
const terser = require('gulp-terser');

gulp.task('stripMinify', () =>
  gulp
    .src(
      [
        'app/**/*.js',
        'database/**/*.js',
        'logger/*.js',
        'exceptions/*.js',
        './index.js',
      ],
      {
        base: '.',
      }
    )
    .pipe(stripDebug())
    .pipe(terser())
    .pipe(gulp.dest('dist'))
);

gulp.task('copy', () =>
  gulp
    .src(['./package.json', './yarn.lock', './ecosystem.config.js'], {
      base: '.',
    })
    .pipe(gulp.dest('dist'))
);
